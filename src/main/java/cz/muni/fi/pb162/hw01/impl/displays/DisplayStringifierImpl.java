package cz.muni.fi.pb162.hw01.impl.displays;

/**
 * implementation of DisplayStringifier interface.
 *
 * @author Adam Paulen
 */
public class DisplayStringifierImpl implements DisplayStringifier{

    private static final String[] DIGIT_CHAR = {
            " _     _  _     _     _  _  _ ",
            "| |  | _| _||_||_ |_   ||_||_|",
            "|_|  ||_  _|  | _||_|  ||_|  |"};

    private static final String[] ERROR_CHAR = {
            " _ ",
            "|_ ",
            "|_ "};

    private static final String[] EMPTY_CHAR = {
            "   ",
            "   ",
            "   "};

    @Override
    public boolean canStringify(Display display) {
        return display instanceof DisplayImpl;
    }

    @Override
    public String[] asLines(Display display) {
        DisplayImpl displayImpl = (DisplayImpl) display;
        int width = displayImpl.getDisplayWidth();
        int height = displayImpl.getDisplayHeight();
        StringBuilder[] lines = new StringBuilder[height];
        for (int i = 0; i < height; i++) {
            lines[i] = new StringBuilder();
        }
        for (char ch : displayImpl.getDisplayText().toCharArray()) {
            int digit = Character.getNumericValue(ch);
            String[] chars = (Character.isDigit(ch)) ? DIGIT_CHAR
                    : (Character.isWhitespace(ch)) ? EMPTY_CHAR
                    : ERROR_CHAR;
            if (chars == EMPTY_CHAR || chars == ERROR_CHAR) {
                digit = 0;
            }
            for (int i = 0; i < height; i++) {
                lines[i].append(chars[i], digit * width, digit * width + width);
            }
        }
        String[] result = new String[height];
        for (int i = 0; i < height; i++) {
            result[i] = lines[i].toString();
        }
        return result;
    }
}
