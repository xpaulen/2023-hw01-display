package cz.muni.fi.pb162.hw01.impl.displays;

/**
 * @author Adam Paulen
 */
public class DisplayImpl implements Display {

    private StringBuilder displayText;
    private int displaySize;
    private int displayWidth;
    private int displayHeight;

    /**
     * Constructor that creates DisplayImpl object
     *
     * @param displaySize size of the display.
     */
    public DisplayImpl(int displaySize) {
        this.displaySize = displaySize;
        clear();
        displayHeight = 3;
        displayWidth = 3;
    }

    /**
     * constructor that creates DisplayImpl object.
     *
     * @param displaySize   size of the display
     * @param displayWidth  display width
     * @param displayHeight display height
     */

    public DisplayImpl(int displaySize, int displayWidth, int displayHeight) {
        this.displaySize = displaySize;
        this.displayWidth = displayWidth;
        this.displayHeight = displayHeight;
        clear();
    }

    @Override
    public void set(String text) {
        displayText.replace(0, text.length(), text);
        displayText.setLength(displaySize);

    }

    @Override
    public void set(int pos, String text) {
        displayText.replace(pos, pos + text.length(), text);
        displayText.setLength(displaySize);
        for (int i = pos + text.length(); i < displaySize; i++) {
            clear(i);
        }
    }

    @Override
    public void clear() {
        displayText = new StringBuilder();
        for (int i = 0; i < displaySize; i++) {
            displayText.append(' ');
        }
    }

    @Override
    public void clear(int pos) {
        displayText.replace(pos, pos + 1, " ");
    }

    /**
     * converts displayText to string
     *
     * @return returns displayText as String
     */
    public String getDisplayText() {
        return displayText.toString();
    }

    public int getDisplayWidth() {
        return displayWidth;
    }

    public int getDisplayHeight() {
        return displayHeight;
    }
}
