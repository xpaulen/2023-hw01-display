package cz.muni.fi.pb162.hw01.impl.app;

import cz.muni.fi.pb162.hw01.cmd.Application;
import cz.muni.fi.pb162.hw01.impl.Factory;
import cz.muni.fi.pb162.hw01.impl.displays.DisplayStringifier;


/**
 * Display application
 */
public class DisplayApp implements Application<DisplayAppOptions> {

    /**
     * Runtime logic of the application
     *
     * @param options options of app
     * @return exit status code
     */
    public int run(DisplayAppOptions options) {
        Factory factory = new Factory();
        DisplayStringifier stringifier = factory.stringifier();
        var display = factory.display(options.getSize());

        display.set(options.getText());
        System.out.println(stringifier.asString(display));
        return 0;
    }
}
